function initialiseForms(){
        var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();       
        var frmEnrolluserLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmEnrolluserLandingKAConfig);
        var frmEnrolluserLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmEnrolluserLandingKAController", appContext, frmEnrolluserLandingKAModelConfigObj);
        var frmEnrolluserLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAControllerExtension", frmEnrolluserLandingKAControllerObj);
        frmEnrolluserLandingKAControllerObj.setControllerExtensionObject(frmEnrolluserLandingKAControllerExtObj);
        var frmEnrolluserLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModel", frmEnrolluserLandingKAControllerObj);
        var frmEnrolluserLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModelExtension", frmEnrolluserLandingKAFormModelObj);
        frmEnrolluserLandingKAFormModelObj.setFormModelExtensionObj(frmEnrolluserLandingKAFormModelExtObj);
        appContext.setFormController("frmEnrolluserLandingKA", frmEnrolluserLandingKAControllerObj);
}

function initMetaData(success, error){
    kony.sdk.mvvm.KonyApplicationContext.init();
    var configParams = {
     "ShowLoadingScreenFunction": ShowLoadingScreen
    };
    var appFactory = kony.sdk.mvvm.KonyApplicationContext.getFactorySharedInstance();
    var appPropertiesInstance = appFactory.createConfigurationServiceManagerObject();
    var appPropertyObj = appPropertiesInstance.generateAppPropsObj(configParams);
    appPropertiesInstance.apply(appPropertyObj);
    var options = "online";
    var initManager = appFactory.createAppInitManagerObject();
    initManager.registerService("MetadataServiceManager",{"object":appFactory.createMetadataServiceManagerObject(),"params":{"options": options}});
    initManager.executeRegistedServices(success, customErrorCallback);
}

function fetchApplicationProperties() {
	ShowLoadingScreen();
   firstTimeSetLanguage();
   initMetaData(success,customErrorCallback);
//   appPropSuccess();
}
function getDeviceInfo() {   //returns an object which contains name(defines the OS) and version number of the OS
          var devInfo = kony.os.deviceInfo();
            var obj = {
                "name": devInfo.name,
                "version":devInfo.version,
                "model":devInfo.model
              };
              return obj;
        }
function success(){ 
  
  kony.print("Initilization complete");
//   generatei18nKeys();
/* var options = {"access":"online"};
 var deviceData = getDeviceInfo();
 var appVersion = appConfig.appVersion;
 objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
 var record = {"OSType":deviceData.name,"OSversion":deviceData.version,"AppVersion":appVersion}; 
 var dataObject = new kony.sdk.dto.DataObject("Application");
 dataObject.setRecord(record);
 var serviceOptions = {"dataObject":dataObject, "headers":""};
 objectService.create(serviceOptions, appPropSuccess,customErrorCallback);*/
  appPropSuccess();
}
function appPropSuccess() 
 {
   kony.retailBanking.globalData.globals.CurrencyCode = "$";//res.currencyCode;
   /*kony.retailBanking.globalData.globals.BankName = res.bankName;
   kony.retailBanking.globalData.applicationProperties.appProperties.businessDays = res.businessDays;
   viewMainBranch(res);
   setBannerUrl(res);
   checkAppUpgrade(res);*/
   firstTimeSetLanguage();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
 }
function setBannerUrl(res){
  var deviceData =  getDeviceInfo();
  var bannerUrl = res.bannerURL+"mobile/banner_mobile";
  if(deviceData.name === "iPhone"){
    if(deviceData.model.indexOf("iPhone 4") > -1)
      bannerUrl = bannerUrl+".png";
    else if(deviceData.model.indexOf("iPhone 5") > -1)
      bannerUrl = bannerUrl+"@2x.png";
    else if(deviceData.model.indexOf("iPhone 6") > -1)
      bannerUrl = bannerUrl+"@3x.png";
  else
       bannerUrl = bannerUrl+"@3x.png";
  }
  else
     bannerUrl = bannerUrl+".9.png";
  if (bannerUrl != null || bannerUrl != "" )
  {
    frmLoginKA.imgBannerKA.src =  bannerUrl;
    frmLoginKA.imgBannerPINKA.src =  bannerUrl;
    frmLoginKA.bannerAd.skin = "sknBannerflx";
    frmLoginKA.bannerAdPinKA.skin="sknBannerflx";
    frmLoginKA.lblNoBanner.setVisibility(false);
    frmLoginKA.lblNoBannerPin.setVisibility(false);
    frmLoginKA.imgBannerKA.setVisibility(true);
    frmLoginKA.imgBannerPINKA.setVisibility(true);
    frmLoginKA.bannerAd.onClick = openBannerAd;
    frmLoginKA.bannerAdPinKA.onClick = openBannerAd;
    
  }
  else
  { 
    frmLoginKA.imgBannerKA.setVisibility(false);
    frmLoginKA.imgBannerPINKA.setVisibility(false);
    frmLoginKA.bannerAd.skin = "sknNoBanner";
    frmLoginKA.bannerAdPinKA.skin="sknNoBanner";
    frmLoginKA.bannerAd.onClick = null;
    frmLoginKA.bannerAdPinKA.onClick = null;
    frmLoginKA.lblNoBanner.setVisibility(true);
    frmLoginKA.lblNoBannerPin.setVisibility(true);
  }

} 

function openBannerAd()
{
  kony.application.openURL('http://pmqa.konylabs.net/KonyWebBanking/campaign.html');
}

function checkAppUpgrade(res){
  if(res.isUpdateMandatory === "true"){
     kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": i18n_upgradeOfApp,
        "yesLabel": i18n_OK,
        "noLabel": "",
        "message": i18n_instrUpgrade,
        "alertHandler": alertCallback
     },{});
  }
  else if(res.isUpdate === "true"){
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": i18n_upgradeOfApp,
        "yesLabel": i18n_upgrade,
        "noLabel": i18n_cancel,
        "message": i18n_qstnUpgrade,
        "alertHandler": alertCallback
    }, {});
  }
}
function alertCallback(response){
  kony.print("response" + response);
    if(response === true)
      kony.application.exit();
  
}

//TouchId-AccountEnable Preview-rememberme

function updateFlags(whichflag,flagvalue)
{
  var deviceflagdata=kony.store.getItem("settingsflagsObject");
  deviceflagdata[whichflag]=flagvalue;
  kony.retailBanking.globalData.globals.settings.whichflag=flagvalue;
  kony.store.setItem("settingsflagsObject",deviceflagdata);
}

function remembeMeOption()
{
   
  var rememberMeflag=frmLoginKA.rememberUsernameSwitch.selectedIndex;
  if (rememberMeflag == 1)
  {
    if (isSettingsFlagEnabled("touchIDEnabledFlag") || isSettingsFlagEnabled("isPinEnabledFlag"))
    	showTouchIdOffAlert(i18n_TouncIDInfoMes,i18n_TouchIDandPINMeg);
    else
       OffLoginFeatures_RememberOff();
  } 
  else
   updateFlags("rememberMeFlag",true);
}

function showTouchIdOffAlert(msg,title)
{
   kony.ui.Alert({
          "message": msg,
          "alertHandler": alertrememberCallback,
          "alertType": constants.ALERT_TYPE_CONFIRMATION,
          "yesLabel": i18n_disable,
          "noLabel": i18n_cancel,
          "alertTitle": title     
     },{});

}
function alertrememberCallback(response)
{
  if (response === true){
    updateFlags("touchIDEnabledFlag",false);
    updateFlags("isPinEnabledFlag",false);
    updateFlags("rememberMeFlag",false);
    manageUname();
    updateFlags("accountPreviewEnabledFlag",false);
   // frmLoginKA.touchIdContainer.setVisibility(false);
    //frmLoginKA.flxPinLoginFeature.setVisibility(false);
  }
  else
  {
    if(kony.retailBanking.globalData.deviceInfo.isIphone())
    	frmLoginKA.rememberUsernameSwitch.selectedIndex=0;
    else
       frmLoginKA.imgRememberme.src = "checkbox_selected.png";
    updateFlags("rememberMeFlag",true);
    }
}

function onclickImageRememberMe()
{
 if (frmLoginKA.imgRememberme.src == "checkbox_selected.png")
  {
    frmLoginKA.imgRememberme.src = "checkbox_unselected.png";
    if(isSettingsFlagEnabled("touchIDEnabledFlag") || isSettingsFlagEnabled("isPinEnabledFlag"))
    	showTouchIdOffAlert(i18n_TouncIDInfoMes,i18n_TouchIDandPINMeg);
    else
      OffLoginFeatures_RememberOff();
  }
 else if(frmLoginKA.imgRememberme.src == "checkbox_unselected.png")
  {
    frmLoginKA.imgRememberme.src = "checkbox_selected.png";
    updateFlags("rememberMeFlag",true);
  }
}

function OffLoginFeatures_RememberOff()
{
    updateFlags("rememberMeFlag",false);
    updateFlags("accountPreviewEnabledFlag",false);
    updateFlags("touchIDEnabledFlag",false);
    updateFlags("isPinEnabledFlag",false);
    frmLoginKA.noThanks.setVisibility(false);
    frmLoginKA.lblChangeUserName.setVisibility(false);
    frmLoginKA.usernameTextField.setVisibility(true);
    if(!isFirstTimeLogin())
    	frmLoginKA.usernameTextField.text="";
}

function firstTimeapplogin()
{
  if(kony.store.getItem("firsttimecheck")=== null)
  {
   kony.retailBanking.datastore.setSettingsObject(kony.retailBanking.globalData.globals.settings);
   kony.store.setItem("settingsflagsObject",kony.retailBanking.globalData.globals.settings);
   kony.store.setItem("firsttimecheck", "finished");
  }
}

function setUserObj(succCalBack){
  kony.print("Perf Log: User information loading Service call- start");
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options ={    "access": "online",
                  "objectName": "RBObjects"
               };
  var headers = {"session_token":kony.retailBanking.globalData.session_token};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("User",serviceName,options);
  var dataObject = new kony.sdk.dto.DataObject("User");
  var serviceOptions = {"dataObject":dataObject, "headers":headers};
  modelObj.fetch(serviceOptions, dataSuccess, customErrorCallback);
  function dataSuccess(response){
	  kony.print("Perf Log: User information loading Service call- End");
      alertsFlag=true;
      if(response[0].alertsTurnedOn==="true"){
         alertsFlag=true;
      }else{
         alertsFlag=false;
      }
      kony.retailBanking.globalData.globals.userObj.userName = response[0].userName;
      kony.retailBanking.globalData.globals.userObj.dateOfBirth = response[0].dateOfBirth;
      kony.retailBanking.globalData.globals.userObj.ssn = response[0].ssn;
      kony.retailBanking.globalData.globals.userObj.email = response[0].email;
      kony.retailBanking.globalData.globals.userObj.phone = response[0].phone;
      kony.retailBanking.globalData.globals.userObj.addressLine1 = response[0].addressLine1;
      kony.retailBanking.globalData.globals.userObj.addressLine2 = response[0].addressLine2;
      kony.retailBanking.globalData.globals.userObj.city = response[0].city;
      kony.retailBanking.globalData.globals.userObj.country = response[0].country;
      kony.retailBanking.globalData.globals.userObj.state = response[0].state;
      kony.retailBanking.globalData.globals.userObj.zipcode = response[0].zipcode;
      kony.retailBanking.globalData.globals.userObj.userFirstName = response[0].userfirstname;
      kony.retailBanking.globalData.globals.userObj.depositsTCaccepted=response[0].areDepositTermsAccepted;
      kony.retailBanking.globalData.globals.settings.alerts=alertsFlag;
      kony.retailBanking.globalData.globals.userObj.acntStatementTCaccepted=response[0].areAccountStatementTermsAccepted;
      kony.retailBanking.globalData.globals.userObj.isPinSet=response[0].isPinSet;
      kony.retailBanking.globalData.globals.userObj.role=response[0].role;
      var appDate = kony.retailBanking.util.formatingDate.getApplicationFormattedDateTime(response[0].lastlogintime, kony.retailBanking.util.BACKEND_DATE_TIME_FORMAT);
      kony.retailBanking.globalData.globals.userObj.lastLoginTime = appDate;
	  kony.store.setItem("lastLoginTime",EncryptValue(kony.retailBanking.globalData.globals.userObj.lastLoginTime));
      kony.retailBanking.globalData.globals.userObj.userLastName =response[0].userlastname;
      var settingsObj = kony.store.getItem("settingsflagsObject");
      settingsObj.alerts = alertsFlag;	
 	  settingsObj.DefaultTransferAcctNo = response[0].default_account_transfers;
 	  settingsObj.DefaultDepositAcctNo = response[0].default_account_deposit;
 	  settingsObj.DefaultPaymentAcctNo = response[0].default_account_payments;
      kony.store.setItem("settingsflagsObject",settingsObj);
      if(succCalBack){
      succCalBack.call(this);
      }
  }
 
}

function enrollUser(userId){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmEnrolluserLandingKA");
  var viewModel = controller.getFormModel();
  var options = {"access":"online"};
  objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
  var q1,q2,q3,q4,q5;
  q1 = viewModel.getWidgetData("lstboxquestn1").getData();
  q2 = viewModel.getWidgetData("lstboxquestn2").getData();
  q3 = viewModel.getWidgetData("lstboxquestn3").getData();
  q4 = viewModel.getWidgetData("lstboxquestn4").getData();
  q5 = viewModel.getWidgetData("lstboxquestn5").getData();
  var x = [{"question_id":q1,"answer":viewModel.getViewAttributeByProperty("tbxAnswer1","text")},
           {"question_id":q2,"answer":viewModel.getViewAttributeByProperty("tbxAnswer2","text")},
           {"question_id":q3,"answer":viewModel.getViewAttributeByProperty("tbxAnswer3","text")},
           {"question_id":q4,"answer":viewModel.getViewAttributeByProperty("tbxAnswer4","text")},
           {"question_id":q5,"answer":viewModel.getViewAttributeByProperty("tbxAnswer5","text")},
          ];
  x = JSON.stringify(x);
  x.replace("\"","'");
  var record = {
                "userId":userId,
                "usersecurityli":x
               };
  var dataObject = new kony.sdk.dto.DataObject("UserSecurityQuestions");
  dataObject.setRecord(record);
  var serviceOptions = {"dataObject":dataObject, "headers":""};
  objectService.create(serviceOptions, enrollSuccess,customErrorCallback);
  
  function enrollSuccess(res){
    alert(res.success);
    frmLoginKA.show();
    kony.print("success question record created");
  }
}

function forgotPasswordFetch(){
  var options = {"access":"online"};
  objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
  var dataObject = new kony.sdk.dto.DataObject("UserSecurityQuestions");
  var usrName = frmUnauthForgotUsernamePasswordStep1KA.txtUserName.text;
  kony.retailBanking.globalData.globals.usrName = usrName;
  var queryParams = {"userName":usrName};
  var serviceOptions = {"dataObject":dataObject,"queryParams":queryParams};
  objectService.fetch(serviceOptions, forgotSuccess,customErrorCallback);
}

function forgotSuccess(res){
  frmUnauthForgotUsernamePasswordKA.lblQuestion1.text = res.records[0].question;
  frmUnauthForgotUsernamePasswordKA.lblId1KA.text = res.records[0].question_id;
  frmUnauthForgotUsernamePasswordKA.lblQuestion2.text = res.records[1].question;
  frmUnauthForgotUsernamePasswordKA.lblId2KA.text = res.records[1].question_id;
  frmUnauthForgotUsernamePasswordKA.lblUserNameKA.text = kony.retailBanking.globalData.globals.usrName;
  frmUnauthForgotUsernamePasswordKA.txtQuestion1.text="";
  frmUnauthForgotUsernamePasswordKA.txtQuestion2.text="";
  frmUnauthForgotUsernamePasswordKA.show();
}
function forgotPassword(){
  var q1 = frmUnauthForgotUsernamePasswordKA.lblId1KA.text;
  var q2 = frmUnauthForgotUsernamePasswordKA.lblId2KA.text;
  var options = {"access":"online"};
  objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
  var x = [{"question_id":q1,"answer":frmUnauthForgotUsernamePasswordKA.txtQuestion1.text},
           {"question_id":q2,"answer":frmUnauthForgotUsernamePasswordKA.txtQuestion2.text}
          ];
  x = JSON.stringify(x);
  x.replace("\"","'");
  var record = {
                "usersecurityli":x
               };
  
  var usrName = kony.retailBanking.globalData.globals.usrName;
  var headers = {"userName":usrName};
  var dataObject = new kony.sdk.dto.DataObject("UserSecurityQuestions");
  dataObject.setRecord(record);
  var serviceOptions = {"dataObject":dataObject, "headers":headers};
  objectService.partialUpdate(serviceOptions, pswrdSuccess,customErrorCallback);
  
 }
function pswrdSuccess(res){
 if(res.errmsg){
   alert(res.errmsg);
 }
  else{
   alert(i18n_securityAnsValidated);
   frmLoginKA.show();
 }
}


function manageUname()
{
  if(!isSettingsFlagEnabled("rememberMeFlag"))
 {
     frmLoginKA.usernameTextField.text="";
     frmLoginKA.passwordTextField.text="";
     if(kony.retailBanking.globalData.deviceInfo.isIphone())
       frmLoginKA.rememberUsernameSwitch.selectedIndex = 1;
     else
      frmLoginKA.imgRememberme.src = "checkbox_unselected.png";
     
   }
  else
    {
      if (kony.store.getItem("firstTimeLogin") !== null)
        frmLoginKA.usernameTextField.text=DecryptValue(kony.store.getItem("userName"));
    }
} 


function removeSwipe()
{
  if(kony.retailBanking.globalData.deviceInfo.isIphone())
          frmLoginKA.removeGestureRecognizer(swipeGesture);
}

function ChangeUserPinFlex()
{
  
      showLoginorPinScreen("pwdLogin");
      frmLoginKA.passwordTextField.text="";
      if(isSettingsFlagEnabled("rememberMeFlag"))
      {
        frmLoginKA.noThanks.setVisibility(true);
        frmLoginKA.lblChangeUserName.setVisibility(true);
        frmLoginKA.usernameTextField.setVisibility(false);
        if(DecryptValue(kony.store.getItem("userName"))!=null)
            frmLoginKA.lblChangeUserName.text=DecryptValue(kony.store.getItem("userName"));
        addTouchIdFeature(); 
      }
      else
        manageUname();
}

function addTouchIdFeature()
{
  if(isSettingsFlagEnabled("touchIDEnabledFlag"))
  		frmLoginKA.touchIdContainer.setVisibility(true);
  else
   // frmLoginKA.touchIdContainer.setVisibility(false);
  if(isSettingsFlagEnabled("isPinEnabledFlag"))
        frmLoginKA.flxPinLoginFeature.setVisibility(true);
  else
    //frmLoginKA.flxPinLoginFeature.setVisibility(false);
  if(isSettingsFlagEnabled("touchIDEnabledFlag") && !isSettingsFlagEnabled("isPinEnabledFlag"))
    frmLoginKA.touchIdContainer.centerX="50%";
  else if(!isSettingsFlagEnabled("touchIDEnabledFlag") && isSettingsFlagEnabled("isPinEnabledFlag"))
    frmLoginKA.flxPinLoginFeature.centerX="50%";
  else
  {
   // frmLoginKA.touchIdContainer.centerX="25%";
    //frmLoginKA.flxPinLoginFeature.centerX="40%";
  }
  //touchLoginShow();
}

function forgotPinNavigation()
{
  showLoginorPinScreen("pwdLogin");
  frmLoginKA.noThanks.setVisibility(false);
  frmLoginKA.lblChangeUserName.setVisibility(false);
  frmLoginKA.usernameTextField.setVisibility(true);
  frmLoginKA.passwordTextField.text="";
  manageUname();
  addTouchIdFeature();
}
//Animation for language selection in login form
var isUpAnimation = false;
var languageSelection;
function selectLanguageForApp(){
  if(frmLoginKA.flxLanguageContainer.isVisible === false){
    isUpAnimation = false;
    languageSelection = frmLoginKA.lblSelectedLanguage.text;
    if(languageSelection == "English"){
      frmLoginKA.lblEnglish.text = "Korean";
    }else{
      frmLoginKA.lblEnglish.text ="English";
    }
    frmLoginKA.flxLanguageContainer.isVisible=true;
    downAnimationLanguageFlx();
  }else{
    isUpAnimation = true;
    var selectedLanguage = frmLoginKA.lblSelectedLanguage.text;
    upAnimationLanguageFlx(selectedLanguage);
  }

}
function selectLanguage(lang){

  isUpAnimation = true;
//   upAnimationLanguageFlx(lang);
  var langMsg;
  if(lang==="English"){
    try{
      kony.i18n.setCurrentLocaleAsync("en", setEngSuccesscallback, onfailurecallback);
      kony.print("current locale is set as "+kony.i18n.getCurrentLocale());
      kony.retailBanking.globalData.language = "English";
      kony.store.setItem("language",kony.retailBanking.globalData.language);
      var crtform = kony.application.getCurrentForm();
//       kony.application.showLoadingScreen("sknLoginBg","", constants.LOADING_SCREEN_POSITION_FULL_SCREEN , true, true, null);
//       crtform.destroy();
//       crtform.show();
//       kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    // langMsg = kony.i18n.getLocalizedString("i18n.common.langChange");
      langMsg = "언어를 변경하고 싶으십니까?";
//       frmLoginKA.lblSelectedLanguage.text = kony.retailBanking.globalData.language;
    }catch (i18nerr){
      alert("Exception in setCurrentLocaleAsync  : "+err );
    }
  }  
  if (lang==="Korean"){
    try{
      kony.i18n.setCurrentLocaleAsync("ko", setKorSuccesscallback, onfailurecallback);
      kony.print("current locale is set as "+kony.i18n.getCurrentLocale());
      kony.retailBanking.globalData.language = "Korean";
      kony.store.setItem("language",kony.retailBanking.globalData.language);
//       var crtform = kony.application.getCurrentForm();
//       kony.application.showLoadingScreen("sknLoginBg","", constants.LOADING_SCREEN_POSITION_FULL_SCREEN , true, true, null);
//       crtform.destroy();
//       crtform.show();
//       kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
     // langMsg = kony.i18n.getLocalizedString("i18n.common.langChange");
      langMsg = "Are sure you want to change the language!";
//       frmLoginKA.lblSelectedLanguage.text = kony.retailBanking.globalData.language;
    }catch (i18nerr){
      alert("Exception in setCurrentLocaleAsync  : "+err );
    }
  }
   	 
       kony.ui.Alert({
            "message": langMsg,
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "",
            "yesLabel": i18n_yes,
            "noLabel": i18n_no,
            "alertIcon": "",
            "alertHandler": userResponse
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });

  function userResponse(response)
  {
    try
    {
       if(response === true){
//            kony.application.exit();
         upAnimationLanguageFlx(kony.retailBanking.globalData.language) ;
         frmSplashKA.show();
         destroyAllFroms();
         generatei18nKeys();
         frmLoginKA.show();
         frmLoginKA.forceLayout();
        }else{
          if(languageSelection == "English"){
            upAnimationLanguageFlx("English") ;
             frmLoginKA.lblEnglish.text = "Korean";
          }else{
            upAnimationLanguageFlx("Korean") ;
             frmLoginKA.lblEnglish.text = "English";
          }
        }
      } 
    catch(err)
    {
      alert(i18n_dailError+" "+err);
    }
  }

}

function downAnimationLanguageFlx(){
  doAnimation("-40dp","35dp");
}

function upAnimationLanguageFlx(lang){
  frmLoginKA.lblSelectedLanguage.text = lang;
  kony.retailBanking.globalData.language = lang;
  doAnimation("35dp","-40dp");
}

function doAnimation(top0,top1){
  frmLoginKA.flxLanguageContainer.animate(
    kony.ui.createAnimation({
      "0":  { "top": top0 ,"stepConfig":{"timingFunction": kony.anim.LINEAR}},
      "100":{ "top": top1 ,"stepConfig":{"timingFunction": kony.anim.LINEAR}}}),
    {"fillMode": kony.anim.FILL_MODE_FORWARDS,"duration": 0.4, "delay": 0},
    {"animationStart": function () {
      if(!isUpAnimation){
        frmLoginKA.flxLanguageContainer.setVisibility(true);
      }
    },
     "animationEnd": function () {
       if(isUpAnimation){
         frmLoginKA.flxLanguageContainer.setVisibility(false);
       }
     }});
}

function firstTimeSetLanguage(){
  kony.print("firstTimeSetLanguage calling : "+kony.store.getItem("language"));
  kony.print("language : "+ kony.retailBanking.globalData.language);
  var getLanguage = kony.store.getItem("language");
  if(kony.store.getItem("language")==="" || kony.store.getItem("language")=== null){
    storeLanguage("en","English");
  }else{
    switch(getLanguage){
      case "Korean": storeLanguage("ko","Korean");
                     break;
      case "English": storeLanguage("en","English");
        			 break;
    } 
  }
}
function storeLanguage(local,language){
  kony.print("Set Local as : "+local+" and language as : "+language);
  kony.i18n.setCurrentLocaleAsync(local, setKorSuccesscallback, onfailurecallback);
  kony.retailBanking.globalData.language = language;
  kony.store.setItem("language",kony.retailBanking.globalData.language);
  var crtform = kony.application.getCurrentForm();
  //crtform.destroy();
  //crtform.show();
}
function setEngSuccesscallback(){
  kony.print("Your Language Changed ");
}
function setKorSuccesscallback(){
  kony.print("Your Language Changed");
}
function onfailurecallback(){
  kony.print("Failure Callback of setCurrentLocal");
}

function initializeMVVMForms() {
  try{
    kony.sdk.mvvm.KonyApplicationContext.init()
    if(userAgent !== "iPhone" && userAgent !== "iPad"){
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      kony.sdk.mvvm.appInit(INSTANCE);
    }else{
      kony.timer.schedule("appInitTimer", invokeAppInit, 0.1, false); 
    }
  }catch(e){
      kony.print("Exception occured in login preshow"+e);
  }
}

function invokeAppInit(){
  try{
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    kony.sdk.mvvm.appInit(INSTANCE);
    kony.print("Cancel timer for app init");
    kony.timer.cancel("appInitTimer");
  }catch(e){
    kony.print("Exception in invokeAppInit "+e);
  }
}

function destroyAllFroms(){
  
  try{
  frmAccountAlertsKA.destroy();
  frmAccountDetailKA.destroy();
  frmAccountInfoEditKA.destroy();
  frmAccountInfoformortageKA.destroy();
  frmAccountInfoKA.destroy();
  frmAccountRecentTransactionDetailsKA.destroy();
  frmAccountsLandingKA.destroy();
  frmAcmeCreditCardKA.destroy();
  frmacntstatementdetailsKA.destroy();
  frmacntstatementsKA.destroy();
  frmAcntTermsAndConditionsKA.destroy();
  frmAddExternalAccountKA.destroy();
  frmAddNewPayeeKA.destroy();
  frmAddPayeeSuccessBillPayKA.destroy();
  frmAddPayeeSuccessKA.destroy();
  frmAlertsKA.destroy();
  frmAndroidMenuKA.destroy();
  frmAnswerSecretQuestionsKA.destroy();
  frmBillDetailsKA.destroy();
  frmCardOperationsKA.destroy();
  frmCheckReorderConfirmationKA.destroy();
  frmCheckReorderDetailsKA.destroy();
  frmCheckReOrderListKA.destroy();
  frmCheckReOrderSuccessKA.destroy();
  frmchequeimages.destroy();
  frmConfirmationCardKA.destroy();
  frmConfirmationPasswordKA.destroy();
  frmConfirmationPersonalDetailsKA.destroy();
  frmConfirmationUserNameKA.destroy();
  frmConfirmDepositKA.destroy();
  frmConfirmPayBill.destroy();
  frmConfirmTransferKA.destroy();
  frmContactUsKA.destroy();
  frmCreditCardsKA.destroy();
  frmDealsAlertKA.destroy();
  frmDepositPayLandingKA.destroy();
  frmDepositSuccessKA.destroy();
  frmDeviceDeRegistrationKA.destroy();
  frmDeviceRegisterationIncorrectPinActicvationKA.destroy();
  frmDeviceregistrarionSuccessKA.destroy();
  frmDeviceRegistrationOptionsKA.destroy();
  frmDeviceRegistrationPinActivationKA.destroy();
  frmDirectionsKA.destroy();
  frmEditPayeeKA.destroy();
  frmEditPayeeSuccessBillPayKA.destroy();
  frmEditPayeeSuccessKA.destroy();
  frmEnrollUserKA.destroy();
  frmEnrolluserLandingKA.destroy();
  frmEnterLocationKA.destroy();
  frmEnterPersonalDetailsKA.destroy();
  frmForgotPasswordKA.destroy();
  frmLocatorATMDetailsKA.destroy();
  frmLocatorBranchDetailsKA.destroy();
  frmLocatorKA.destroy();
  frmLoginKA.destroy();
  frmManageCardsKA.destroy();
  frmManagePayeeKA.destroy();
  frmMessageDetailKA.destroy();
  frmMessageDraftKA.destroy();
  frmMessageReplyKA.destroy();
  frmMoreCheckReorderKA.destroy();
  frmMoreFaqKA.destroy();
  frmMoreForeignExchangeRatesKA.destroy();
  frmMoreInterestRatesKA.destroy();
  frmMoreLandingKA.destroy();
  frmMorePrivacyPolicyKA.destroy();
  frmMoreTermsAndConditionsKA.destroy();
  frmMyMessagesKA.destroy();
  frmMyMoneyListKA.destroy();
  frmNewBillKA.destroy();
  frmNewCheckReOrderKA.destroy();
  frmNewDepositBackOverlay.destroy();
  frmNewDepositFrontOverlay.destroy();
  frmNewDepositKA.destroy();
  frmNewMessageKA.destroy();
  frmNewPayPersonKA.destroy();
  frmNewTransferKA.destroy();
  frmP2PaddnewPayeeKA.destroy();
  frmP2PConfirmationPayKA.destroy();
  frmP2PConfirmTransferKA.destroy();
  frmP2PphonepayeeKA.destroy();
  frmP2PselectPayeeKA.destroy();
  frmPayeeDetailsKA.destroy();
  frmPayeeTransactionsKA.destroy();
  frmPickAProductKA.destroy();
  frmPinEditFromSettings.destroy();
  frmPinEntryStep1.destroy();
  frmPinEntryStep2.destroy();
  frmPinEntrySuccess.destroy();
  frmPreferredAccountsKA.destroy();
  frmRecentDepositKA.destroy();
  frmRecentTransactionDetailsKA.destroy();
  frmScheduledTransactionDetailsKA.destroy();
  frmSearchOptionsKA.destroy();
  frmSearchResultsKA.destroy();
  frmSearchTransactionDetailsKA.destroy();
  frmSecurityAlertsKA.destroy();
  frmSetDefaultPageKA.destroy();
  frmSetLocatorDistanceFilterKA.destroy();
  frmSetUpPinSettings.destroy();
  frmSuccessFormKA.destroy();
  frmTANDCforEnroll.destroy();
  frmTermsAndConditionsAccountsKA.destroy();
  frmTermsAndConditionsBillPayKA.destroy();
  frmTermsAndConditionsKA.destroy();
  frmtransactionChequeKA.destroy();
  frmTransactionDetailKA.destroy();
  frmTransactionDetailsPFMKA.destroy();
  frmTransferPayLandingKA.destroy();
  frmUnauthFeatureEnablingAccPreview.destroy();
  frmUnauthFeatureEnablingKA.destroy();
  frmUnauthForgotUsernamePasswordKA.destroy();
  frmUnauthForgotUsernamePasswordStep1KA.destroy();
  frmUnauthInformationKA.destroy();
  frmUncategorizedTransactionsKA.destroy();
  frmUpdateAccountAlertsKA.destroy();
  frmUpdatePinfromSettings.destroy();
  frmUserSettingsAccountPreviewKA.destroy();
  frmUserSettingsEditPasswordKA.destroy();
  frmUserSettingsEditPersonalDetailsKA.destroy();
  frmUserSettingsEditPersonalDetailsSecurityKA.destroy();
  frmUserSettingsEditUsernameKA.destroy();
  frmUserSettingsKA.destroy();
  frmUserSettingsMyProfileKA.destroy();
  frmUserSettingsPasswordKA.destroy();
  frmUserSettingsPersonalDetailsKA.destroy();
  frmUserSettingsTouchIdKA.destroy();
  frmUserSettingsUsernameKA.destroy();
  frmViewApplicationKA.destroy();
  }catch(e){
  kony.print("Exception in destroyAllFormsFunction"+e);
  }
}